import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';

export enum ROUTE_PATH {
  DASHBOARD = 'dashboard',
  LOGIN = 'login',
  GRID = 'grid',
  MENU = 'menu',
  FORGOTPASSWORD = 'forgot-password'
}

const routes: Routes = [
  { path: '', redirectTo: '/' + ROUTE_PATH.DASHBOARD, pathMatch: 'full' },
  { path: ROUTE_PATH.DASHBOARD, component: DashboardComponent },
  { path: ROUTE_PATH.LOGIN, component: LoginComponent },
  { path: ROUTE_PATH.MENU, component: MenuBarComponent },
  { path: ROUTE_PATH.FORGOTPASSWORD, component: ForgotPasswordComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
