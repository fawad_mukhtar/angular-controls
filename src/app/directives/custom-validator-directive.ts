import { Directive, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[customValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: CustomValidatorDirective, multi: true }]
})
export class CustomValidatorDirective implements Validator {
  @Input() public customValidator: (formControl: FormControl) => { [key: string]: any }

  public constructor() { }

  public validate(formControl: FormControl): { [key: string]: any } {
    console.log(formControl)
    console.log(this.customValidator(formControl))
    return this.customValidator(formControl)
  }
}
