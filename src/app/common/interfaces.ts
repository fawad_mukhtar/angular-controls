import { template } from "@angular/core/src/render3";

export interface IGridOptions {
  enableFilter?: boolean,
  enablePagination?: boolean,
  enableSorting?: boolean,
  enableSelection: boolean,
  multipleSelection: boolean,
  enableRowExpansion?: boolean
}
