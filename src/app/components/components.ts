import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register/register.component';
import { RequiredButtonComponent } from './required-button/required-button.component';
import { RequiredSelectComponent } from './required-select/required-select.component';
import { RequiredInputComponent } from './required-input/required-input.component';
import { FormErrorsComponent } from './form-errors/form-errors';
import { GridUiComponent } from './grid-ui/grid-ui.component';
export const components = [
  DashboardComponent,
  LoginComponent,
  MenuBarComponent,
  ForgotPasswordComponent,
  RegisterComponent,
  RequiredButtonComponent,
  RequiredSelectComponent,
  RequiredInputComponent,
  FormErrorsComponent,
  GridUiComponent,
];
