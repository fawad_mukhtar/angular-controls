export const INPUT_DATA = {
  requiredSelect: {
    listOptions: [
      { name: 'Fawad Mukhtar', value: 1 },
      { name: 'Fawad Mukhtar', value: 2 },
      { name: 'Fawad Mukhtar', value: 3 },
      { name: 'Fawad Mukhtar', value: 4 },
      { name: 'Fawad Mukhtar', value: 5 },
      { name: 'Fawad Mukhtar', value: 6 },
      { name: 'Fawad Mukhtar', value: 7 },
      { name: 'Fawad Mukhtar', value: 8 }]
  },

  grid: {
    options: {
      enableFilter: true,
      enablePagination: true,
      enableSorting: true,
      enableSelection: true,
      multipleSelection: true,
      enableRowExpansion: true
    },
    gridColunms: ['select', 'position', 'name', 'weight', 'symbol'],
    data: [
      {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H',
        template: `<div class="row">
                    <div class="col-md-4">
                      <ul>
                        <li>item one</li>
                        <li>item two</li>
                        <li>item three</li>
                      </ul>
                    </div>
                    <div class="col-md-4">This is coloum</div>
                    <div class="col-md-4">This is coloum</div>
                  </div>`
      },
      { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He', template: `<div>This is expanded </div>` },
      { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li', template: `<div>This is expanded </div>` },
      { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be', template: `<div>This is expanded </div>` },
      { position: 5, name: 'Boron', weight: 10.811, symbol: 'B', template: `<div>This is expanded </div>` },
      { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C', template: `<div>This is expanded </div>` },
      { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N', template: `<div>This is expanded </div>` },
      { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O', template: `<div>This is expanded </div>` },
      { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F', template: `<div>This is expanded </div>` },
      { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne', template: `<div>This is expanded </div>` },
    ]
  }
}
