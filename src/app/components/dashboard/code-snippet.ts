export const CODE_SNIPPETS = {
  requiredSelect: `
                  <required-select-ngx
                    classes="col-md-9"
                    placeholder="- Select -"
                    errorMessage="This is error message."
                    showHelptext="true"
                    helptext="This is info message."
                    labelText="User"
                    elementName="select-user"
                    [(model)]="listOptions[0].value"
                    [options]="listOptions"
                    (change)="someMethod()">
                  </required-select-ngx>

                  .ts file
                  listOptions: [
                    { name: 'Fawad Mukhtar', value: 1 },
                    { name: 'Fawad Mukhtar', value: 2 },
                    { name: 'Fawad Mukhtar', value: 3 },
                    { name: 'Fawad Mukhtar', value: 4 },
                    { name: 'Fawad Mukhtar', value: 5 },
                    { name: 'Fawad Mukhtar', value: 6 },
                    { name: 'Fawad Mukhtar', value: 7 },
                    { name: 'Fawad Mukhtar', value: 8 }]`,
  requiredInput: `
                <required-input-ngx
                  *ngIf="isControlActive('requiredInput')"
                  classes="col-xs-4"
                  labelText="Required Input"
                  elementName="required-input"
                  [(model)]="requiredInput"
                  maxLength="80"
                  [errorMessages]="['This is error 1']">
                </required-input-ngx>`,
  login: `
                <app-login-form
                  (submitLoginForm)="onSubmit($event)"
                  (forgotPasswordLink)="forgotPassword()"
                  [isShowCredentialsErrorMsg]="isLoggingFailed">
                </app-login-form>`,
  register: `
                <app-register
                  (submitedRegisterForm)="registerFormSubmission($event)">
                </app-register>`,

  grid: `
                <app-grid-ui
                  *ngIf="isControlActive('grid')"
                  [gridSourceData]="grid.data"
                  [gridColumnsName]="grid.gridColunms"
                  [gridOptions]="grid.options">
                </app-grid-ui>

                // .ts file

                grid: {
                  options: {
                    enableFilter: true,
                    enablePagination: true,
                    enableSorting: true,
                    enableSelection: true,
                    multipleSelection: true,
                    enableRowExpansion: true
                  },
                  gridColunms: ['select', 'position', 'name', 'weight', 'symbol'],
                  data: [
                    {
                      position: 1,
                      name: 'Hydrogen',
                      weight: 1.0079,
                      symbol: 'H',
                      template: <div class="row">
                                  <div class="col-md-4">
                                    <ul>
                                      <li>item one</li>
                                      <li>item two</li>
                                      <li>item three</li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4">This is coloum</div>
                                  <div class="col-md-4">This is coloum</div>
                                </div>
                    },
                    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He', template: <div>This is expanded </div> },
                    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li', template: <div>This is expanded </div> },
                    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be', template: <div>This is expanded </div> },
                    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B', template: <div>This is expanded </div> },
                    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C', template: <div>This is expanded </div> },
                    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N', template: <div>This is expanded </div> },
                    { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O', template: <div>This is expanded </div> },
                    { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F', template: <div>This is expanded </div> },
                    { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne', template: <div>This is expanded </div> },
                  ]
                }`,

}
