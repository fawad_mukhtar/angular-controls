import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_PATH } from '../../app-routing.module';
import { CODE_SNIPPETS } from './code-snippet'
import { INPUT_DATA } from './input-data-for-controls'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public isLoggingFailed = false;
  public selectedSnippet: string
  public requiredInput = 'Fawad Mukhtar'

  public controls = [
    {
      displayName: 'Login Modal',
      aliasName: 'login',
      isActive: true,
    },
    {
      displayName: 'Grid Modal',
      aliasName: 'grid',
      isActive: false,
    },
    {
      displayName: 'Register User Model',
      aliasName: 'register',
      isActive: false,
    },
    {
      displayName: 'Required Select',
      aliasName: 'requiredSelect',
      isActive: false,
    },
    {
      displayName: 'Required Input',
      aliasName: 'requiredInput',
      isActive: false,
    },
  ]

  public requireSelectControl = INPUT_DATA.requiredSelect
  public grid = INPUT_DATA.grid
  constructor(private router: Router) { }

  ngOnInit() {
  }

  public displaySelectedControl(aliasName: string) {
    this.controls.forEach(control => {
      control.isActive = control.aliasName === aliasName
    })

    this.selectedSnippet = CODE_SNIPPETS[aliasName]
  }

  public onSubmit(formData): void {
    console.log(formData);
    // lets credentials are incorrect
    this.isLoggingFailed = true;
  }

  public registerFormSubmission(formData): void {
    console.log(formData);
  }
  public forgotPassword(): void {
    this.router.navigateByUrl(ROUTE_PATH.FORGOTPASSWORD);
  }

  public isControlActive(aliasName: string): boolean {
    return this.controls.find(control => control.aliasName === aliasName).isActive
  }

  public someMethod(): void { }
}
