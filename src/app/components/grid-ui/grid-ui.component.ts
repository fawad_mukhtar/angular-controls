import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { IGridOptions } from '../../common/interfaces';
import { SelectionModel } from '@angular/cdk/collections';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-grid-ui',
  templateUrl: './grid-ui.component.html',
  styleUrls: ['./grid-ui.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class GridUiComponent<T> implements OnInit {
  @Input() public gridOptions: IGridOptions;
  @Input() public gridSourceData: Array<T>;
  @Input() public gridColumnsName: Array<string>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public dataSource: MatTableDataSource<T>
  public selection: SelectionModel<Array<T>>
  public expandedRow: T | null;
  public constructor() { }
  public ngOnInit() {
    this.dataSource = new MatTableDataSource(this.gridSourceData);
    this._initializeGridOptions();
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private _initializeGridOptions(): void {
    if (this.gridOptions.enablePagination) {
      this.dataSource.paginator = this.paginator;
    }

    if (this.gridOptions.enableSorting) {
      this.dataSource.sort = this.sort
    }

    if (this.gridOptions.enableSelection) {
      this.selection = new SelectionModel<Array<T>>(this.gridOptions.multipleSelection, [])
    }

  }

  public isAllSelected(): boolean {
    return this.dataSource.data.length === this.selection.selected.length
  }

  public masterToggle(): void {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach((row: any) => this.selection.select(row));
  }

  public clicked(event): void {
    console.log(event)
  }

}
