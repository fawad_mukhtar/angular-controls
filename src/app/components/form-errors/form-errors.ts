import { Component, Input } from '@angular/core'
import { AbstractControl } from '@angular/forms'
import * as _ from 'lodash'

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'form-errors',
  templateUrl: './form-errors.html'
})
export class FormErrorsComponent {
  @Input() public formComponent: AbstractControl
  @Input() public errorMessages: {[key: string]: string}

  public getErrorKeys(): string[] {
    const {ngbDate: ngbDateErrors, ...otherErrors} = this.formComponent.errors

    return [
      ..._.keys(otherErrors),
      // We want any ngbDate error keys to be handled by the ngFor as well
      ..._.keys(ngbDateErrors)
    ]
  }
}
