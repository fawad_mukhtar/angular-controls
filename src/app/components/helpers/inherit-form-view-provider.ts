import { Optional, Provider } from '@angular/core';
import { ControlContainer, NgForm, } from '@angular/forms';

export function controlContainerFactory(controlContainer?: ControlContainer): ControlContainer {
  return controlContainer;
}

/**
 * Needed so a child component can automatically be attached to a parent's form
 *
 * Usage:
 *
 * @Component({
 *   selector: ...
 *   template: ...
 *   viewProviders: [inheritFormViewProvider()],
 * })
 */
export function inheritFormViewProvider(): Provider {
  return {
    provide: ControlContainer,
    useFactory: controlContainerFactory,
    deps: [[new Optional(), NgForm]]
  }
}
