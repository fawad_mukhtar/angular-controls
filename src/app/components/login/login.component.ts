import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  @Output() submitLoginForm: EventEmitter<any> = new EventEmitter();
  @Input() public isShowCredentialsErrorMsg: boolean;
  @Output() forgotPasswordLink: EventEmitter<any> = new EventEmitter();

  public loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.loginForm = this.createForm();
  }

  public get username() {
    return this.loginForm.get('username');
  }

  public get password() {
    return this.loginForm.get('password');
  }

  public onSubmit(): void {
    this.submitLoginForm.emit(this.loginForm.value);
  }

  private createForm(): FormGroup {
    return this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  public forgotPassword() {
    this.forgotPasswordLink.emit();
  }
}
