import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredButtonComponent } from './required-button.component';

describe('RequiredButtonComponent', () => {
  let component: RequiredButtonComponent;
  let fixture: ComponentFixture<RequiredButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiredButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
