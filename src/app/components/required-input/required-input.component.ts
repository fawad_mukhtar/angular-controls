import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core'
import { AbstractControl, NgModel, ValidationErrors } from '@angular/forms'

import { inheritFormViewProvider } from 'src/app/components/helpers/inherit-form-view-provider'

/**
 * An Angular-specific version is needed to ensure form validation works when used outside of Angular JS
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'required-input-ngx',
  templateUrl: './required-input.component.html',
  styleUrls: ['./required-input.component.scss'],
  viewProviders: [inheritFormViewProvider()],
})

export class RequiredInputComponent implements OnInit {
  @Input() public classes: string
  private modelValue: any
  @Input() public get model(): any {
    return this.modelValue
  }
  public set model(value: any) {
    if (this.modelValue === value) {
      return
    }
    this.modelValue = value
    this.modelChange.emit(this.modelValue)
  }

  @Input() public placeholder: string
  @Input() public customValidator: (formControl: AbstractControl) => ValidationErrors
  @Input() public labelText: string
  @Input() public errorMessages: {[key: string]: string}
  @Input() public elementName: string
  @Input() public inputType: string
  @Input() public max: number
  @Input() public min: number
  @Input() public maxlength: number
  @Input() public isDisabled: boolean

  @Output() public change = new EventEmitter<void>()
  @Output() public modelChange = new EventEmitter<any>()

  @ViewChild('field') public field: NgModel

  public constructor(
  ) {
  }

  public ngOnInit(): void {
    this.customValidator = this.customValidator || (() => ({}))
    if (!this.inputType) {
      this.inputType = 'text'
    }
  }

  public get mergedErrorMessages(): {[key: string]: string} {
    return {
      required: 'This field is required',
      ...this.errorMessages
    }
  }

  public hasErrors(): boolean {
    return !!(this.field.errors && (this.field.dirty || this.field.touched))
  }
}
