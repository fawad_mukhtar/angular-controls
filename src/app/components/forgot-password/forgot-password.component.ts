import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ROUTE_PATH } from '../../app-routing.module';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  public forgotPasswordForm: FormGroup;

  public constructor(private formBuilder: FormBuilder, private router: Router) {
    this.forgotPasswordForm = this.createForm();
  }

  public get email() {
    return this.forgotPasswordForm.get('email');
  }

  private createForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  public onSubmit() {
    alert('Your request has been received. If it is a valid email, you should receive an e-mail to reset your password soon.');
  }

  public backToLogin() {
    this.router.navigateByUrl(ROUTE_PATH.DASHBOARD);
  }
}
