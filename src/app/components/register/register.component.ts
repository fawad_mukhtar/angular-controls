import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Output() submitedRegisterForm: EventEmitter<any> = new EventEmitter()
  public registerForm: FormGroup;
  public errorMessage: string = '';
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.createForm();
  }

  public get username() {
    return this.registerForm.get('username');
  }

  public get password() {
    return this.registerForm.get('password');
  }
  public get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }
  public get firstName() {
    return this.registerForm.get('firstName');
  }
  public get lastName() {
    return this.registerForm.get('lastName');
  }

  public onSubmit(): void {
    this.errorMessage = '';

    if (!this.isPasswordmatch()) {
      this.errorMessage = 'Password is not matach. Please write same password.';
      return
    }

    this.submitedRegisterForm.emit(this.registerForm.value);
    this.registerForm.reset();
  }

  private createForm(): FormGroup {
    return this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });
  }

  private isPasswordmatch(): boolean {
    console.log(this.registerForm.get('password'))
    return this.registerForm.get('password').value === this.registerForm.get('confirmPassword').value;
  }

}
