import { Component, EventEmitter, Input, Output, OnInit, ViewChild, } from '@angular/core'
import { NgModel } from '@angular/forms'
import { inheritFormViewProvider } from 'src/app/components/helpers/inherit-form-view-provider'
import * as _ from 'lodash'
// import { Utilities } from '~/shared/angular/services/utilities/utilities'
// import { ILookupItem } from '~/shared/type_defs/app.interfaces'

/**
 * An Angular-specific version is needed to ensure form validation works when used outside of Angular JS
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'required-select-ngx',
  templateUrl: './required-select.component.html',
  viewProviders: [inheritFormViewProvider()],
})
export class RequiredSelectComponent implements OnInit {
  @Input() public classes: string
  private modelValue: any
  @Input() public get model(): any {
    return this.modelValue
  }
  public set model(value: any) {
    if (this.modelValue === value) {
      return
    }
    this.modelValue = value
    this.modelChange.emit(this.modelValue)
  }
  @Input() public elementName: string
  @Input() public placeholder: string
  @Input() public labelText: string
  @Input() public errorMessage: string
  @Input() public options: any
  @Input() public noValidate: boolean
  @Input() public isDisabled: boolean
  @Input() public helptext: string
  @Input() public showHelptext: boolean

  @Output() public change = new EventEmitter<number>()
  @Output() public modelChange = new EventEmitter<number>()

  @ViewChild('selectField') public selectField: NgModel

  private selectedModel: any

  public constructor(
    // public Utilities: Utilities,
  ) {
  }

  public hasErrors(): boolean {
    return !!(this.selectField && this.selectField.errors && (this.selectField.touched || this.selectField.dirty))
  }

  public ngOnInit(): void {
    this.model = _.isNil(this.model)
      ? this.model
      : Number(this.model)

    this.classes = this.classes || ''
  }

  public update(): void {
    this.model = this.selectedModel.value
  }

  public doOnChange(newValue: number): void {
    this.change.emit(newValue)
  }
}
