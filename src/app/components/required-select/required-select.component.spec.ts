import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredSelectComponent } from './required-select.component';

describe('RequiredSelectComponent', () => {
  let component: RequiredSelectComponent;
  let fixture: ComponentFixture<RequiredSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiredSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
